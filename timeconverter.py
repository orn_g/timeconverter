from datetime import datetime
import argparse

class TimeRegistration:
    def __init__(self, hours, args):
       self.hours = hours 
       self.date = datetime.strptime(args[0], '%d.%m.%Y')
       self.dateString = args[0]
       self.inTime = args[1]
       self.outTime = args[2]
       self.comment = ' '.join(args[3:])
    

def get_hours_diff(time1, time2):
    delta = abs(time2 - time1)
    return delta.seconds / 3600.0 - 0.33 #convert to hours and subtract half an hour, except we don't subtract half an hour because we allow for time to get to and from parking lot

def parse_file_line(line):
    args = line.split()
    if len(args) < 3:
        #print("Unable to parse line: {line}".format(line=line))
        return None
    #date = args[0]
    t1 = argument_time(args[1])
    t2 = argument_time(args[2])
    return TimeRegistration(get_hours_diff(t1, t2), args)

def argument_time(str_time):
    try:
        return datetime.strptime(str_time, "%H:%M")
    except ValueError as e:
        raise argparse.ArgumentTypeError(e)

def calculate_statistics(registrations):
    hours = 0
    count = 0
    for registration in registrations:
        hours += registration.hours
        #saturday = 5, sunday = 6
        if registration.date.weekday() < 5:
            count += 1
    overtime = hours - (count * 7.5)
    average = hours/len(registrations)
    return (hours, average, overtime)

def main():
    parser = argparse.ArgumentParser(description="Convert time registrations in file to hours per day.")
    parser.add_argument("file", type=argparse.FileType('r'), help="If this argument is provided, times will be read from a file.")
    args = parser.parse_args()

    registrations = []
    for line in args.file:
        strippedLine = line.strip()
        if strippedLine == "":
            continue
        if strippedLine.startswith('#'):
            print(strippedLine)
            continue
        timeRegistration = parse_file_line(strippedLine)
        if timeRegistration is None:
            print(line)
        else:
            registrations.append(timeRegistration)
            print("{date}\t{in_time}\t{out_time}\t{hours}:{minutes:02.0f}\t{comment}".format(
                date=timeRegistration.dateString,
                in_time=timeRegistration.inTime,
                out_time=timeRegistration.outTime,
                #time=timeRegistration.hours,
                hours=int(timeRegistration.hours),
                minutes=(timeRegistration.hours % 1) * 60,
                comment=timeRegistration.comment)
           )

    total, average, overtime = calculate_statistics(registrations)
    print("Total: %.2f" % total)
    print("%.2f per day" % average)
    print("Overtime: %.2f " % overtime)

if __name__ == "__main__":
    main()

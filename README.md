# README #

Really simple script for converting simple txt file registrations into hours along with monthly overview.

Note: The script subtracts 0.5 hours for lunch when calculating times.

### How do I get set up? ###

* Grab the code: Navigate to the folder where you want it and run `git clone https://bitbucket.org/orn_g/timeconverter.git`
* Create a text file in the correct format (see below)
* run `python timeconverter [yourtextfile]`

### Text file format ###

The textfile should contain lines with one day of time registration per line.
Each line should start with the date followed by the starting time and ending time, separated by whitespace.
Anything else included in a line is ignored but printed out as a comment.
The time format for start and ending times should be `HH:MM`. The format for the dates `dd.mm.yyyy`, e.g. `23.08.2018`.
Lines which start with # are ignored but printed out by the script.

#### Example ####

Example text file (example.txt)
```
01.12.2017  08:30	16:30	
### Week end ###
04.12.2017  08:10	16:24	
05.12.2017  08:45	17:13	
```

Output of `python timeconverter example.txt`

```
01.12.2017  08:30	16:30   7.50
### Week end ###
04.12.2017  08:10	16:24	7.73
05.12.2017  08:45	17:13	7.97
Total: 23.20
7.73 per day
Overtime: 0.70
```
